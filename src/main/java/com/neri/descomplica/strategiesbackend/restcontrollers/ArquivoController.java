package com.neri.descomplica.strategiesbackend.restcontrollers;

import com.neri.descomplica.strategiesbackend.entities.Arquivo;
import com.neri.descomplica.strategiesbackend.services.ArquivoService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * @author pedro.neri
 * @project strategies-backend
 * @email pedro.neri@
 * @createat 11/05/2023
 */
@RestController
@RequestMapping("api/files")
@AllArgsConstructor
public class ArquivoController {


    private ArquivoService service;


    @PostMapping("/upload")
    public Arquivo uploadFile(@RequestParam("file") MultipartFile file) {
        String fileName = service.insertFile(file);

        String filePath = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/files/downloadFile/")
                .path(fileName)
                .toUriString();

        return new Arquivo(fileName, filePath, file.getContentType(), file.getSize());
    }

    @GetMapping("/downloadFile/{fileName}")
    public ResponseEntity<Resource> downloadFile(@PathVariable ("fileName") String fileName, HttpServletRequest request) {

        Resource resource = service.carregarFile(fileName);
        String contentType = service.getContentType(request, resource);

        return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; fileName=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
