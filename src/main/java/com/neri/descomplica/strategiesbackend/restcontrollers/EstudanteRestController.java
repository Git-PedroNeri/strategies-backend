package com.neri.descomplica.strategiesbackend.restcontrollers;

import com.neri.descomplica.strategiesbackend.entities.Estudante;
import com.neri.descomplica.strategiesbackend.services.EstudanteService;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author pedro.neri
 * @project strategies-backend
 * @email pedro.neri@
 * @createat 11/05/2023
 */
@RestController
@RequestMapping("api/estudantes")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EstudanteRestController {

    private EstudanteService service;

    @Autowired
    public EstudanteRestController(EstudanteService estudanteService) {
        this.service = estudanteService;
    }

    @GetMapping()
    public ResponseEntity<Page<Estudante>> findAll(@RequestParam(defaultValue = "0") Integer page,
                                                   @RequestParam(defaultValue = "5") Integer size ) {
        return service.findAll(PageRequest.of(page,size));
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Estudante> findById(@PathVariable("id") Long id) {
        return service.findById(id);
    }

    @PostMapping("/insert")
    public ResponseEntity<Estudante> insert(@RequestBody Estudante estudante) {
        return service.insert(estudante);

    }

    @PostMapping("/insertAll")
    public ResponseEntity<Estudante> insert(@RequestBody List<Estudante> estudantes) {
        return service.insertInBach(estudantes);

    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Estudante> update(@PathVariable("id") Long id) {
        return service.update(id);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {
        return service.delete(id);
    }


}
