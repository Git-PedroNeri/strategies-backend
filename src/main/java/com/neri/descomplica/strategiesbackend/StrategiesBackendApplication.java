package com.neri.descomplica.strategiesbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class StrategiesBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(StrategiesBackendApplication.class, args);
    }

    @GetMapping("/path-variable/bem-vindo/{nome}")
    public String utilizarPathVariable(@PathVariable(value = "nome",required = false) String nome) {
        return "Welcome to Las Vegas ".concat(nome);
    }

    @GetMapping("/request-param/bem")
    public String utilizarRequestParam(@RequestParam("nome") String nome,
                                       @RequestParam("idade")int idade,
                                       @RequestParam(value = "snome",required = false)String snome){
        return nome+" "+snome+" "+ idade;
    }

}
