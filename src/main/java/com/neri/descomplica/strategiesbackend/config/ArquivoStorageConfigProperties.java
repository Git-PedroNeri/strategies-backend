package com.neri.descomplica.strategiesbackend.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author pedro.neri
 * @project strategies-backend
 * @email pedro.neri@
 * @createat 11/05/2023
 */

@Component
@Getter
public class ArquivoStorageConfigProperties {
    @Value("${arquivo.uploadDir}")
    private String uploadDir;

}
