package com.neri.descomplica.strategiesbackend.repositories;

import com.neri.descomplica.strategiesbackend.entities.Estudante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author pedro.neri
 * @project strategies-backend
 * @email pedro.neri@
 * @createat 12/05/2023
 */
@Repository
public interface EstudanteRepository extends JpaRepository<Estudante, Long> {
}
