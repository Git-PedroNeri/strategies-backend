package com.neri.descomplica.strategiesbackend.services;

import com.neri.descomplica.strategiesbackend.entities.Estudante;
import com.neri.descomplica.strategiesbackend.repositories.EstudanteRepository;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author pedro.neri
 * @project strategies-backend
 * @email pedro.neri@
 * @createat 11/05/2023
 */

@Service
@AllArgsConstructor
public class EstudanteService {

    EstudanteRepository repository;

    public ResponseEntity<Estudante> findById(Long id) {

        if (repository.existsById(id))
            return ResponseEntity.status(HttpStatus.FOUND).body(repository.findById(id).get());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    public ResponseEntity<Page<Estudante>> findAll(Pageable pageable) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(repository.findAll(pageable));
    }

    public ResponseEntity<Estudante> insert(Estudante estudante) {
        if (estudante.getId() == null) {
            Estudante save = repository.save(estudante);
            return ResponseEntity.status(HttpStatus.CREATED).body(save);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(null);
        }
    }


    public ResponseEntity<Estudante> insertInBach(List<Estudante> estudantes) {
        List<Estudante> estudantes1 = repository.saveAllAndFlush(estudantes);
        return ResponseEntity.status(HttpStatus.CREATED).build();

    }

    public ResponseEntity<Estudante> update(@NonNull Long id) {
        Optional<Estudante> byId = repository.findById(id);
        if (byId.isPresent()) {
            Estudante atualizado = repository.save(byId.get());
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(atualizado);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    public ResponseEntity<String> delete(Long id) {
        Optional<Estudante> encontrado = repository.findById(id);
        if (encontrado.isPresent()) {
            repository.deleteById(encontrado.get().getId());
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("removed!");
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }


}
