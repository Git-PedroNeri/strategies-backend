package com.neri.descomplica.strategiesbackend.services;

import com.neri.descomplica.strategiesbackend.config.ArquivoStorageConfigProperties;
import com.neri.descomplica.strategiesbackend.exceptions.UploadFileExceptionHandler;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * @author pedro.neri
 * @project strategies-backend
 * @email pedro.neri@
 * @createat 11/05/2023
 */
@Service
@Slf4j
public class ArquivoService {

    private final Path fileStorageLocation;

    public ArquivoService(ArquivoStorageConfigProperties arquivoStorageConfigProperties) {

        String pathFile = arquivoStorageConfigProperties.getUploadDir();
        this.fileStorageLocation = Paths.get(pathFile).toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (IOException e) {
            throw new UploadFileExceptionHandler("Algo deu Errado", e);
        }
    }


    public String insertFile(MultipartFile file) {
        String nameFile = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            if (nameFile.contains("..")) {
                throw new UploadFileExceptionHandler("Arquivo invalido");
            }
            Path targetLocation = this.fileStorageLocation.resolve(nameFile);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return nameFile;

        } catch (IOException e) {

            log.error("erro ao tentar inserir arquivo");
            throw new UploadFileExceptionHandler("Erro ao tentar sakver arquivo");
        }

    }

    public Resource carregarFile(String nameFile) {

        try {
            Path filePath = this.fileStorageLocation.resolve(nameFile).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                log.error("File not found");
                throw new UploadFileExceptionHandler("Arquivo n encontrado");
            }

        } catch (IOException e) {
            log.error("File not found");
            throw new UploadFileExceptionHandler("Arquivo não encontrado");
        }

//        try {
//
//            if (nameFile.contains("..")) {
//                throw new UploadFileExceptionHandler("Arquivo invalido");
//            }
//            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
//            return nameFile;
//
//        } catch (IOException e) {
//
//            log.error("erro ao tentar inserir arquivo");
//            throw new UploadFileExceptionHandler("Erro ao tentar sakver arquivo");
//        }

    }


    /**
     * Tipo do arquivo
     *
     * @param request
     * @param resource
     * @return
     */
    public String getContentType(HttpServletRequest request, Resource resource) {
        String contentType = null;

        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());

        } catch (IOException e) {
            log.error("Não foi possível determinar o tipode arquivo");
        }

        if (contentType == null) {
            contentType = "application?ovtet-stream";
        }
        return contentType;

    }


}
