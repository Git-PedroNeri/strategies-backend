package com.neri.descomplica.strategiesbackend.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author pedro.neri
 * @project strategies-backend
 * @email pedro.neri@
 * @createat 11/05/2023
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Arquivo {

    private String nome;
    private String urlDownload;
    private String tipoArquivo;
    private long size;

}
