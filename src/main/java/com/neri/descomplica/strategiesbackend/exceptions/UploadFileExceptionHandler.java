package com.neri.descomplica.strategiesbackend.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author pedro.neri
 * @project strategies-backend
 * @email pedro.neri@
 * @createat 11/05/2023
 */

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Arquivo não encontrado")
public class UploadFileExceptionHandler extends RuntimeException {
    public UploadFileExceptionHandler(String message) {
        super(message);
    }
    public UploadFileExceptionHandler(String message, Throwable cause) {
        super(message,cause);
    }
}
